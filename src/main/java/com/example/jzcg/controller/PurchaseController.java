package com.example.jzcg.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.jzcg.commonutils.R;
import com.example.jzcg.entity.Purchase;
import com.example.jzcg.ro.QueryListRo;
import com.example.jzcg.service.PurchaseService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ReactiveAdapterRegistry;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.Iterator;
import java.util.List;

@RestController
@Slf4j
@RequestMapping("/purchase")
@CrossOrigin
public class PurchaseController {
    @Autowired
    private PurchaseService purchaseService;

    //导入数据
    @PostMapping("importPurchase")
    public R addPurchaseResult(@RequestParam("file") MultipartFile file) {
        try{
            purchaseService.savePurchaseResult(file,purchaseService);
            return R.ok();
        }catch (Exception e){
            e.printStackTrace();
            return R.error();
        }
    }

    //查询数据
    @PostMapping("/queryPurchase")
    public R queryPurchaseResult(@RequestBody QueryListRo queryListRo){
        try{
            log.info(queryListRo.toString());
            Page pageInfo = purchaseService.queryPurchaseResult(queryListRo);
            List<Purchase> records = pageInfo.getRecords();
            long total = pageInfo.getTotal();
            return R.ok().data("records",records).data("total",total);
        }catch (Exception e){
            e.printStackTrace();
            return R.error();
        }
    }

    //删除数据
    @PostMapping("/deletePurchase")
    public R deletePurchasResult(@RequestBody String projectId){
        int i = purchaseService.deletePurchaseResult(projectId);
        if(i>0){
            return R.ok();
        }else{
            return R.error();
        }
    }

    //更新数据
    @PostMapping("/updatePurchase")
    public R updatePurchasResult(@RequestBody Purchase purchase){
        int i = purchaseService.updatePurchaseResult(purchase);
        if(i>0){
            return R.ok();
        }else{
            return R.error();
        }
    }

    //根据id查询
    @PostMapping("/queryPurchaseById")
    public R queryPurchaseResultById(@RequestBody String projectId){
        Purchase purchase = purchaseService.queryPurchaseResultById(projectId);
        if(purchase!=null){
            return R.ok().data("purchase",purchase);
        }
        else{
            return R.error();
        }
    }
}
