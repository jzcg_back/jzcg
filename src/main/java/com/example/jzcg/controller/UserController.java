package com.example.jzcg.controller;

import com.example.jzcg.commonutils.R;
import com.example.jzcg.entity.User;
import com.example.jzcg.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/user")
@CrossOrigin
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("findAll")
    public R findAllTeacher(){
        List<User> users = userService.findAllUsers();
        return R.ok().data("users",users);
    }
}
