//package com.example.jzcg.controller;
//import com.baomidou.mybatisplus.core.conditions.Wrapper;
//import com.baomidou.mybatisplus.core.conditions.segments.MergeSegments;
//import com.example.jzcg.commonutils.R;
//import com.example.jzcg.entity.Purchase;
//import com.example.jzcg.mapper.DelAltMapper;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.*;
//
//import java.io.Serializable;
//import java.sql.SQLException;
//
//@RestController
//@RequestMapping("/purchase")
//@CrossOrigin
//public class DelAltController {
//    @Autowired
//    private DelAltMapper delAltMapper;
//
//    @PostMapping("/deletePurchase")
//    public R deleteProduct(@RequestBody Purchase purchase){
//        Wrapper<Purchase> w = new Wrapper<Purchase>() {
//            @Override
//            public Purchase getEntity() {
//                return null;
//            }
//
//            @Override
//            public MergeSegments getExpression() {
//                return null;
//            }
//
//            @Override
//            public String getSqlSegment() {
//                return null;
//            }
//        };
//        w.equals(purchase);
//        delAltMapper.delete(w);
//        try{
//            return R.ok();
//        }catch (Exception e){
//            System.out.println("出错了...");
//            return R.error();
//        }
//    }
//
//    @PostMapping("/updatePurchase")
//    public R altProduct(@RequestBody Purchase purchase){
//        delAltMapper.update(purchase,null) ;
//        try{
//            return R.ok();
//        }catch (Exception e){
//            System.out.println("出错了...");
//            return R.error();
//        }
//    }
//}
