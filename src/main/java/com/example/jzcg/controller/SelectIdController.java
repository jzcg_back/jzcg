package com.example.jzcg.controller;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.segments.MergeSegments;
import com.example.jzcg.commonutils.R;
import com.example.jzcg.entity.Purchase;
import com.example.jzcg.mapper.DelAltMapper;
import com.example.jzcg.mapper.SelectIdMapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.GetMapping;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/purchase")
@CrossOrigin

public class SelectIdController {
    @Autowired
    private SelectIdMapper selectIdMapper;

    @GetMapping(value = "/findById")
    public R findById(@RequestParam String projectId)
    {

        Purchase user = selectIdMapper.selectById(projectId);
//        int i=0;
        try{
            return R.ok().data("purchase",user);
        }catch (Exception e){
            System.out.println("出错了...");
            return R.error();
        }
    }

}
