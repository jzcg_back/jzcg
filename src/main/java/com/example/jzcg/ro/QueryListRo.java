package com.example.jzcg.ro;

import com.example.jzcg.entity.Purchase;
import lombok.Data;

@Data
public class QueryListRo {
    private int pageNum;
    private int pageSize;
    private String projectId;
    private String productName;
    private String projectCategory;
    private String productCategory;
    private String brand;
    private String supplier;
    private String introduction;
    private char order;//升序还是降序
    private String sortby;//排序字段名

}
