package com.example.jzcg.listener;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.example.jzcg.entity.Purchase;
import com.example.jzcg.service.PurchaseService;

public class PurchaseExcelListener extends AnalysisEventListener<Purchase> {

    public PurchaseService purchaseService;

    public PurchaseExcelListener(){}

    public PurchaseExcelListener(PurchaseService purchaseService){
        this.purchaseService = purchaseService;
    }

    @Override
    public void invoke(Purchase purchase, AnalysisContext analysisContext) {
        purchaseService.saveOrUpdate(purchase);
    }

    @Override
    public void doAfterAllAnalysed(AnalysisContext analysisContext) {

    }
}
