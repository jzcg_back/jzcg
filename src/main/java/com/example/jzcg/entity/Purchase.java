package com.example.jzcg.entity;

import com.alibaba.excel.annotation.ExcelProperty;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@TableName("purchase_result")
@Data
public class Purchase {
    @ExcelProperty(value = "项目编号",index = 0)
    @TableId(type = IdType.INPUT)
    private String projectId;

    @ExcelProperty(value = "项目名称",index = 1)
    private String projectName;

    @ExcelProperty(value = "项目分类",index = 2)
    private String projectCategory;

    @ExcelProperty(value = "产品分类",index = 3)
    private String productCategory;

    @ExcelProperty(value = "产品名称",index = 4)
    private String productName;

    @ExcelProperty(value = "品牌",index = 5)
    private String brand;

    @ExcelProperty(value = "说明",index = 6)
    private String introduction;

    @ExcelProperty(value = "单位",index = 7)
    private String unit;

    @ExcelProperty(value = "单价",index = 8)
    private float unitPrice;

    @ExcelProperty(value = "供应商",index = 9)
    private String supplier;

    @ExcelProperty(value = "联系人",index = 10)
    private String linkman;

    @ExcelProperty(value = "联系电话",index = 11)
    private String telephone;

    @ExcelProperty(value = "签订日期",index = 12)
    private String signDate;

    @ExcelProperty(value = "终止日期",index = 13)
    private String endDate;

}
