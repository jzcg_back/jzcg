package com.example.jzcg.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.jzcg.entity.User;

import javax.jws.soap.SOAPBinding;
import java.util.List;

public interface UserService{
    public List<User> findAllUsers();
}
