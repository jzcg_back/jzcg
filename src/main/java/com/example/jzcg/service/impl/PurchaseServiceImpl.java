package com.example.jzcg.service.impl;

import com.alibaba.excel.EasyExcel;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.jzcg.entity.Purchase;
import com.example.jzcg.listener.PurchaseExcelListener;
import com.example.jzcg.mapper.PurchaseMapper;
import com.example.jzcg.ro.QueryListRo;
import com.example.jzcg.service.PurchaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.InputStream;


@Service
public class PurchaseServiceImpl extends ServiceImpl<PurchaseMapper, Purchase> implements PurchaseService {

    @Autowired
    private PurchaseMapper purchaseMapper;

    @Override
    public void savePurchaseResult(MultipartFile file, PurchaseService purchaseService) throws Exception{
        //文件输入流
        InputStream in = file.getInputStream();
        //调用方法进行读取
        EasyExcel.read(in, Purchase.class,new PurchaseExcelListener(purchaseService)).sheet().doRead();
    }

    @Override
    public Page queryPurchaseResult(QueryListRo queryListRo) throws Exception{
        String projectId = queryListRo.getProjectId();
        String productName = queryListRo.getProductName();
        String projectCategory = queryListRo.getProjectCategory();
        String productCategory = queryListRo.getProductCategory();
        String brand = queryListRo.getBrand();
        String supplier = queryListRo.getSupplier();
        String introduction = queryListRo.getIntroduction();
        int pageNum = queryListRo.getPageNum();
        int pageSize = queryListRo.getPageSize();
        char order = queryListRo.getOrder();
        String sortby = queryListRo.getSortby();

        //构造条件构造器
        Page purchasePage = new Page<>(pageNum,pageSize);
        QueryWrapper<Purchase> queryWrapper = new QueryWrapper<>();

        queryWrapper.eq(projectId!=null && projectId!="","project_id",projectId);
        queryWrapper.eq(projectCategory!=null && projectCategory!="","project_category",projectCategory);
        queryWrapper.eq(productCategory!=null && productCategory!="","product_category",productCategory);
        queryWrapper.eq(productName!=null && productName!="","product_name",productName);
        queryWrapper.eq(brand!=null && brand!="","brand",brand);
        queryWrapper.eq(introduction!=null && introduction!="","introduction",introduction);
        queryWrapper.eq(supplier!=null && supplier!="","supplier",supplier);
        //添加排序条件
        if (order == 'D'){
            queryWrapper.orderByDesc(sortby);
        } else if (order == 'A') {
            queryWrapper.orderByAsc(sortby);
        }
        page(purchasePage,queryWrapper);
        return purchasePage;
    }

    @Override
    public int deletePurchaseResult(String id) {
        int i=-1;
        try {
            i = purchaseMapper.deleteById(id);
        }catch (Exception e){
            e.printStackTrace();
        }
        return i;
    }

    @Override
    public int updatePurchaseResult(Purchase purchase) {
        int i = -1;
        try{
            i = purchaseMapper.updateById(purchase);
        }catch (Exception e){
            e.printStackTrace();
        }
        return i;
    }

    @Override
    public Purchase queryPurchaseResultById(String id) {
        Purchase purchase = purchaseMapper.selectById(id);
        return purchase;
    }
}
