package com.example.jzcg.service.impl;

import com.example.jzcg.entity.User;
import com.example.jzcg.mapper.UserMapper;
import com.example.jzcg.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper userMapper;

    @Override
    public List<User> findAllUsers() {
        List<User> users = userMapper.selectList(null);
        return users;
    }
}
