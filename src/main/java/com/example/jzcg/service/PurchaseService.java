package com.example.jzcg.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.example.jzcg.entity.Purchase;
import com.example.jzcg.ro.QueryListRo;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface PurchaseService extends IService<Purchase> {

    //导入采购结果
    public void savePurchaseResult(MultipartFile file, PurchaseService purchaseService) throws Exception;

    //查询数据
    public Page queryPurchaseResult(QueryListRo queryListRo) throws Exception;

    //删除数据
    public int deletePurchaseResult(String id);

    //更新数据
    public int updatePurchaseResult(Purchase purchase);

    //根据id查询
    public Purchase queryPurchaseResultById(String id);
}
