package com.example.jzcg.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.jzcg.entity.User;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Repository
public interface UserMapper extends BaseMapper<User> {
}
