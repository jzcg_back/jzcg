package com.example.jzcg.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.jzcg.entity.Purchase;
import org.springframework.stereotype.Repository;

@Repository
public interface DelAltMapper extends BaseMapper<Purchase> {

}
