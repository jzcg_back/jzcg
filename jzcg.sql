/*
 Navicat Premium Data Transfer

 Source Server         : test
 Source Server Type    : MySQL
 Source Server Version : 80028
 Source Host           : localhost:3306
 Source Schema         : jzcg

 Target Server Type    : MySQL
 Target Server Version : 80028
 File Encoding         : 65001

 Date: 14/07/2023 21:30:23
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

use jzcg;
create table if not exists `jzcg_tb` (
	`project_id` varchar(25) not null,
    `project_name` varchar(255),
    `project_category` varchar(10),
    `product_category` varchar(10),
    `product_name` varchar(50),
    `brand` varchar(10),
    `introduction` varchar(255),
    `unit` varchar(10),
    `unit_price` float(2),
    `supplier` varchar(50),
    `linkman` varchar(10),
    `telephone` varchar(30),
    `sign_date` varchar(20),
    `end_date` varchar(20),
    primary key (`project_id`)
    )ENGINE=InnoDB DEFAULT CHARSET=utf8;
